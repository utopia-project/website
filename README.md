# Utopia Website

This repository contains the Utopia website, which uses the [Hugo](https://gohugo.io) framework to build the website from the current state of the repository.
The (soon to become) publicly available webpage is deployed using [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/), and can be found at http://utopia-project.org.

To let changes to the `master` branch go live, they need to be *manually deployed*.
To do so, click on the ▶️ button in the corresponding pipeline, the CI/CD interface, or the [environments](https://gitlab.com/utopia-project/website/-/environments) page.

You can *preview* your changes to a branch with an associated merge request via the Merge Requests interface's "View in App" button.
The preview of the master branch will always be generated.


### Building the website locally
You can also build the website locally. To do this:

1. **Install [Hugo](https://gohugo.io)**:
    ```bash
    brew install hugo
    ```
    
    See [here](https://gohugo.io/getting-started/installing/) for details and troubleshooting guidelines.

2. **Clone this repository**:
    ```bash
    git clone REPO-CLONE-URL
    ```

3. **Install Git LFS (Large File Storage)**: since the repo contains a number of large image files, we use [Git LFS](https://gohugo.io/getting-started/installing/) for more efficient file storage.
   Large files are replaced with text pointers inside Git, and the files themselves are stored on a remote server.
   Follow the Git LFS installation instructions, then do 
    ```bash
    git lfs install
    ```
    
    If you wish to add further files to the LFS, do
    ```bash
    git lfs track "*.FILE-EXT" # e.g. "*.psd"
    ```

4. Finally, **to build the site from your local repo**, navigate to your local "website" folder and do 
    ```bash
    hugo server
    ```
    The website is then available at http://localhost:1313 and will auto-refresh upon file changes.
