<!-- Issue template for content or design bugs -->

### I observed ...
<!-- Description of the bug -->

<!-- For design bugs, please upload a screen shot. -->
<!-- Please do this via the "Upload Design" feature, __after__ issue creation -->


### I expected ...
<!-- Description of what you expected to see or read -->


### Ideas how to fix this?
<!-- Especially if it's a design issue,  -->


<!-- Please label appropriately with ~design or ~content -->
/label ~bug
