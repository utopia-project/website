This MR ...

### Can this MR be accepted?
- [ ] Implemented the changes
- [ ] Checked webpage: everything looks as intended

If there changes to the theme: <!-- Delete, if not -->
- [ ] Checked *all* affected pages: everything looks good
- [ ] Reviewed and approved

Closes ...
