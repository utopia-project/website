---
date: 2020-04-16T

tagline: GameofLife
heading: Conway's Game of Life
subheading: >
  This model implements John Conway's famous [Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) as well as all two-dimensional [life-like cellular automata](https://en.wikipedia.org/wiki/Life-like_cellular_automaton).

type: models
highlight: false

cover_image: "images/models/GoL/GoL_1.png"
#video: "https://seafile.blsqr.xyz/d/3bb1ecb0f0364026bbc8/files/?p=/videos/GameOfLife.mp4&raw=1"
project_images:
  images:
    - link: "images/models/GoL/GoL_1.png"
      desc:
    - link: "images/models/GoL/GoL_2.png"
  desc: Model state snapshots

link_to_documentation: https://docs.utopia-project.org/html/models/GameOfLife.html

---

The game is a grid-based cellular automaton, completely pre-determined by its
initial state. Each cell can be either *alive* or *dead*, and in each iteration step
interacts with its eight neighbors via the following four rules:

1. A living cell with fewer than two living neighbors dies.
1. A living cell with more than three living neighbors dies.
1. A living cell with two or three living neighbors continues living.
1. A dead cell with exactly three living neighbors becomes a living cell.

The game is an example of a self-organising system, allowing for complex patterns
to emerge. It is also Turing complete.
