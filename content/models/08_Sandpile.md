---
date: 2020-04-16T

tagline: SandPile
heading: "Model of Sandpile formation"
subheading: >
  The `SandPile` model simulates the development of a sand pile to which a grain of sand is added at each time step. At a critical slope level, the sandpile relaxes and distributes grains onto the surrounding cells.

type: models
highlight: false

cover_image: "/images/models/SandPile/SandPile_1.png"
video: ""
project_images:
  images:
    - link: "/images/models/SandPile/SandPile_1.png"
      desc: >
        Exemplary avalanches in the `SandPile` model, triggered by adding one unit at a randomly chosen cell of a 512 x 512 grid.
  desc: >
    Exemplary avalanches in the `SandPile` model, triggered by adding one unit at a randomly chosen cell of a 512 x 512 grid.

link_to_documentation: https://docs.utopia-project.org/html/models/SandPile.html

---

The model is initialized with a random distribution of sand, with the slope exceeding the critical value everywhere. It is then relaxed (sand topples) such that the model ends the initial iteration step with a slope below the critical slope everywhere. In each further iteration, a grain of sand is added at a random position. If the slope exceeds the critical value, sand topples to the neighboring positions. If this leads to a neighboring position exceeding the critical slope, the sand topples again. This goes on during each time step until the slope is below the critical value everywhere. The borders of the model’s grid are fixed at a given (sub-critical) slope value, such that each sand may “fall off” the borders of the grid.

In each time step, the rearrangement of grains can affect any number of positions. It is possible that the slope only changes in one position, or that a single grain added causes an avalanche that affects almost the entire grid. These features of unpredictability and lack of characteristic scale make the model interesting.
