---
date: 2020-04-16T

tagline: Opinionet
heading: Model of Social Network Opinion Dynamics
subheading: >
  `Opinionet` is a model of opinion dynamics on a social network. It simulates a network of users, who update their opinions upon interaction with their social contacts. This model includes a variety of different opinion dynamics models from the literature, including the Hegselmann-Krause (2002) and Deffuant (2000) models. The one-dimensional opinion space can be chosen to be discrete or continuous, and network edge weights can be configured to represent interaction probabilities. The `Opinionet` model is designed for further expansion and adaptation by researchers of social network opinion dynamics.

type: models
highlight: true

cover_image: "images/models/Opinionet/Opinionet_graph_yellow.png"
video: "https://seafile.blsqr.xyz/d/3bb1ecb0f0364026bbc8/files/?p=/videos/Opinionet.mp4&raw=1"
project_images:
  images:
    - link: "images/models/Opinionet/Opinionet_graph_white.png"
      desc:
    - link: "images/models/Opinionet/Opinionet_distr.png"
      desc:
  desc: Snapshot of a small-world social network, with user opinions color-coded (*left*)
        and the corresponding opinion distribution on a continuous opinion space (*right*).

link_to_documentation: https://docs.utopia-project.org/html/models/Opinionet.html

---

#### Implementation

**Opinion space**

The network consists of nodes (representing users) and edges (representing social links). Each user holds an opinion, a value in the one-dimensional opinion space. In the model, the opinion space can be either continuous or discrete. A continuous opinion space represents views held on a spectrum with two opposing extremes (e.g. ranging from ‘yes’ to ‘no’, ‘agree’ to ‘disagree’, etc.), and is modeled as a real interval *[a, b]*; a discrete space holds views on a topic with a countable number of distinct choices (e.g. either ‘yes’ or ‘no’, different preferences for political parties, etc.), and is modeled as a series of integers *{0, 1, ..., N}*.

**Interaction functions**

There are two basic types of interaction functions: the **Deffuant** type, and the **Hegselmann-Krause** type. In addition, two parameters are relevant to the opinion interaction: the *tolerance*
 and the *susceptibility*.

The tolerance determines which spectrum of opinions a user is willing to engage with. A small tolerance bound means users only interact with opinions very close to their own; a large tolerance means users are rather more ‘broad-minded’.

The susceptibility, in general, models how susceptible users are to others’ opinions, that is, to what degree they are willing to shift their own opinion towards that of others.

In the Hegselmann-Krause model, users shift their opinions towards the average opinion of all of their neighbors. In the Deffuant model, in contrast, only pairs of users interact.

**Network**

In each iteration of the model, a single user is chosen at random with uniform probability, and this user’s opinion is updated through its interaction with other users. Which users interact is determined through the network topology. Utopia allows choosing [different types of underlying topologies](https://docs.utopia-project.org/html/usage/implement/graph.html#graph-gen-functions) for the network (e.g. small-world or scale-free), or [loading your own network from a dataset](https://docs.utopia-project.org/html/usage/implement/graph.html#loading-a-graph-from-a-file). The network can be directed or undirected, and edges can be given weights, representing the probability of the end nodes interacting.

**Edge rewiring**

The topology of the network does not have to be static. You can let users cut links and rewire to new neighbors via the rewiring option. If activated, a randomly selected link between users whose opinions are further apart than the tolerance is rewired to a new, randomly chosen neighbor. All these dynamics can easily be adapted to your own custom model.

**Plotting**

`Opinionet` comes with several configurable default plots:

- **Graph plots** allow you to plot a single snapshot of the network, or an animated plot of the network over time.

- Also included are: an animated histogram of the opinion distribution over time, a plot showing the temporal development of the opinion density, the final opinion distribution, as well as some representative trajectories.

- Several **Multiverse plots** for parameter sweep runs are also included. Various data analytical parameters can be plotted for multiverse runs, e.g. as a 1d errorbar, or a 2d heatmap. These plots use the [data transformation framework](https://dantro.readthedocs.io/en/latest/data_io/transform.html).

---

**References**

- Deffuant G. et al: *Mixing beliefs among interating agents.* Adv Complex Syst. (2000) **3**:87-98.
- Hegselmann, R. & Krause, U. (2002). *Opinion Dynamics and Bounded Confidence Models, Analysis and Simulation.* J. Artificial Societies and Soc. Simulation **5** 3: 1–33.
- Gaskin, Thomas (2020). Master’s thesis: *Modelling Homophily and Discrimination in Selective Exposure Opinion Dynamics.* Heidelberg University. Download [here](https://1drv.ms/b/s!AnHsRv-O4KyKg_B2pPTiCFEBrFthww?e=LCb2jj).
- Traub, Jeremias (2019). Bachelor’s thesis: *Modelling Opinion Dynamics – Selective Exposure in Adaptive Social Networks.* Heidelberg University.
