---
date: 2020-04-16T

tagline: SimpleEG
heading: Model of Simple Evolutionary Games
subheading: >
  This is a model of simple evolutionary games (on regular grids). It is based on an extended and slightly modified version of Nowak & May (1992).
type: models
highlight: true
cover_image: "images/models/SimpleEG/SimpleEG_2.png"
video: "https://seafile.blsqr.xyz/d/3bb1ecb0f0364026bbc8/files/?p=/videos/SimpleEG.mp4&raw=1"
project_images:
  images:
    - link: "images/models/SimpleEG/SimpleEG_3.png"
      desc: "Snapshot from the 'Kaleidoscope' configuration"
    - link: "images/models/SimpleEG/SimpleEG_2.png"
      desc:
  desc:
    Model state snapshots
link_to_documentation: https://docs.utopia-project.org/html/models/SimpleEG.html
---

The world is a regular square grid of cells. Each cell represents one player that possesses two internal properties: a strategy and a payoff. The strategy determines how the player interacts with its neighbors. Thus, the basic interaction is pair-wise. However, the neighborhood of a player, which is defined through the grid, consists of multiple players, such that that multiple games are played per iteration step. The resulting gain from the individual games are summed up and stored as each player’s payoff.

The payoff a player receives from a single game is encoded in the interaction matrix, where each matrix element `i, j` denotes the payoff a player with strategy `i` receives when playing against a player with strategy `j`. In this game, there are two possible strategies. In the case of a Prisoner's Dilemma, these strategies correspond to *cooperation* and *defection*. Here, we want to be able to model all kind of two-dimensional two-player games, hence the more general nomenclature.

#### Neighborhood

The neighbors of a player placed on a regular square grid are determined by the Moore neighborhood (9-neighborhood) of the corresponding cell. Thus, every player has 9 neighbours against which she plays in a single iteration step.

#### Evolution

Evolution is modelled by a very simple imitation update scheme. After all games are played, every player looks in her neighborhood for the player with the highest payoff, thus, the player with the most successful strategy in her corresponding neighborhood. If this highest payoff is higher than the player's payoff, she adapts her strategy to that of the more successful neighbor. This means that, in the next round, the player will participate in all the individual games with the strategy that locally yielded the best result in the previous round.

If more than one player has the highest fitness in a neighborhood, one of these is selected randomly to pass on her strategy.

#### Iteration steps

An interaction step consists of two stages:


1. **Play the games:** every player plays with all players in her neighborhood
   the game defined by the interaction matrix. The payoffs of the games are
   summed up and stored as a property of the players.

2. **Update the strategies:** each player looks within its neighborhood for
   the most successful strategy (highest payoff) and adapts this strategy for
   the next iteration step.

These two steps are repeated for the desired number of iteration steps.
