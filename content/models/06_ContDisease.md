---
date: 2020-04-16T

tagline: ContDisease
heading: "Model of Contagious Diseases"
subheading: >
  A simple model of contagious disease spreading on a two-dimensional grid.

type: models
highlight: false

cover_image: "/images/models/ContDisease/ContDisease_1.png"
video: ""
project_images:
  images:
    - link: "/images/models/ContDisease/ContDisease_2.png"
      desc:
    - link: "/images/models/ContDisease/ContDisease_1.png"
      desc:

  desc: Infection clusters in the `ContDisease` model

link_to_documentation: https://docs.utopia-project.org/html/models/ContDisease.html

---

The model implements a "forest" on a two-dimensional square grid of cells.
Each cell can be in one of five different states: *empty*, *tree*, *infected*, *source*, or *stone*.

### Implementation

#### Update Rules

In each time step, the cells update their respective states according to the
following rules:

1. An ``infected`` cell turns into an ``empty`` cell.
2. An ``empty`` cell can become a ``tree`` cell with probability ``p_growth``.
3. A ``tree`` cell can become infected in the following ways:

   - from a neighboring infected cell with probability 1-``p_immunity``
     *per neighbor*,
   - via a random point infection with probability ``p_infect``,
   - via a constantly infected cell, an infection ``source``.

For the neighborhood, both the von Neumann neighborhood (5-neighborhood) and the Moore neighborhood (9-neighborhood) are supported.

#### Heterogeneities

As in the [Forest Fire model]((/models/04_forestfire/index.html)), there is the possibility to introduce heterogeneities into the grid, which are implemented as two additional possible cell states:

* ``source``: these are constant infection sources. They spread infection like normal infected trees, but don not revert back to the empty state. If activated, they are per default on the lower boundary of the grid, though this can be changed in the configuration.
* ``stone``: stones are cells that can not be infected nor turn into trees. They are used to represent barriers in the forest. If enabled, the default mode is ``clustered_simple``, which leads to randomly distributed stones whose neighbours have a certain probability to also be a stone.

Both make use of the [entity selection interface](https://docs.utopia-project.org/html/about/features.html#the-select-interface-selecting-entities-using-some-condition).

#### Infection Control

Via the ``infection_control`` parameter in the model configuration, additional infections can be introduced at desired times. The infections are introduced before the update rule above is carried out.
