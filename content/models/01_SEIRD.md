---
date: 2020-04-16T

tagline: SEIRD
heading: "Model of Infectious Diseases"
subheading: >
  This is a simple model combining concepts and ideas from the well-known [SEIR (Susceptible-Exposed-Infected-Recovered)](https://en.wikipedia.org/wiki/Compartmental_models_in_epidemiology#The_SEIR_model>) and [SIRD (Susceptible-Infected-Recovered-Deceased)](https://en.wikipedia.org/wiki/Compartmental_models_in_epidemiology#The_SIRD_model>) models, and  adapting them to a spatial 2D grid model.

type: models
highlight: true

cover_image: "images/models/SEIRD/SEIRD_1.png"
video: "https://seafile.blsqr.xyz/d/3bb1ecb0f0364026bbc8/files/?p=/videos/SEIRD.mp4&raw=1"
project_images:
  images:
    - link: "images/models/SEIRD/SEIRD_1.png"
      desc: "Infection spreading in the SEIRD model (grid resolution: 512 x 512)."
    - link: "images/models/SEIRD/SEIRD_2.png"
      desc: "Infection clusters in the SEIRD model (grid resolution: 512 x 512)."
  desc: |
    Model state snapshots: infection spreading (*left*) and infection clusters
    (*right*)

link_to_documentation: https://docs.utopia-project.org/html/models/SEIRD.html

---

### Fundamentals

We model a population of agents on a two-dimensional grid of cells that can move randomly or away from infected agents. Each cell can be in one of the following states:

- ``empty``: there is no agent on the cell.
- ``susceptible``: the agent on the cell is healthy but susceptible to the disease and can be exposed if in contact with the disease.
- ``exposed``: the agent on the cell is exposed to the disease, meaning that it can already infect other agents in contact with the disease; however, there are no symptoms yet, thus, the agent gets noticed as infected only after the incubation period.
- ``infected``: the agent on the cell is infected and can infect neighboring agents.
- ``recovered``: the agent on the cell is recovered, thus, it is immune to the disease. However, it can lose its immunity after a while and become susceptible again.
- ``deceased``: the agent on the cell is deceased. The cell will again be empty in the next time step.

#### Special Cell States

Additionally, cells can also have the following "special" states:

- ``source``: the cells are infection sources, thus, they can transition neighboring agents from the susceptible to the exposed state.
- ``inert``: inert cells are cells that do not partake in *any* of the model dynamics.
  They can be used to model spatial heterogeneities like compartmentalization, much like ``stones`` in the [ForestFire](/models/04_forestfire/index.html) model.

Cells that are initialized in one of these states should not be regarded as representing agents:
there is no movement for these cells, nor can these cells change their state.


### Implementation

The implementation allows for a range of different storylines by changing the parameters.
Keep in mind that individual processes can often be disabled by setting probabilities to zero.

#### Update Rules

In each time step the cells update their respective states asynchronously, but randomly shuffled to reduce artefacts, according to the following rules:

1. A living (susceptible, exposed, infected, or recovered) cell becomes empty with probability ``p_empty``.
2. An ``empty`` cell turns into a ``susceptible`` one with probability ``p_susceptible``. With probability ``p_immune`` the cell is ``immune`` to being infected.
3. A ``susceptible`` cell either becomes randomly exposed with probability ``p_exposed`` or becomes exposed with probability ``p_transmit * (1 - p_random_immunity)`` if a neighboring cell is ``exposed`` or ``infected``, with ``p_transmit`` being the neighboring cell's probability of transmitting the disease. Disease transmission happens only if the cell is _not_ ``immune``.
4. An ``exposed`` cell becomes ``infected`` with probability ``p_infected``.
5. An ``infected`` cell recovers with probability ``p_recovered`` and becomes ``immune``, becomes ``deceased`` with probability ``p_deceased``, or else stays infected.
6. A ``recovered`` cell can lose its immunity with ``p_lose_immunity`` and becomes ``susceptible`` again.
7. A ``deceased`` cell turns into an ``empty`` cell.

#### Movement

In each time step, the agents on the cells can move to ``empty`` neighboring cells according to the following rules:

1. A living (susceptible, exposed, infected, or recovered) cell moves with probability ``p_move_randomly`` to a randomly chosen ``empty`` neighboring cell, if there is any.
2. A living cell moves away from an ``infected`` neighboring cell to a randomly selected neighboring ``empty`` cell if there is any.


#### Heterogeneities

As in the [Forest Fire model](/models/04_forestfire/index.html), there is the possibility to introduce heterogeneities into the grid that are implemented as two additional possible cell states:

* ``source``: these are constant exposure sources.
  They spread the infection like normal infected or exposed cells, but don not revert to the empty state.
  If activated, they are per default at the lower boundary of the grid, though this can be changed in the configuration.
* ``inert``: inert cells are cells that do not partake in the dynamics of the model, and hence can be used to represent barriers.
  If enabled, the default mode is ``clustered_simple``, which leads to randomly distributed inert cells whose neighbors have a certain probability to also be inert.

Both make use of the [entity selection interface](https://docs.utopia-project.org/html/about/features.html#the-select-interface-selecting-entities-using-some-condition).

#### Immunity Control

Via the ``immunity_control`` parameter in the model configuration, additional immunities can be introduced at desired times, thereby manipulating a cell's ``immune`` state.
This feature can be used e.g. to investigate the effect of vaccination. The immunities are introduced *before* the update rule above is carried out.


#### Exposure Control

Via the ``exposure_control`` parameter in the model configuration, additional exposures can be introduced at desired times. The exposures are introduced *before* the update rule above is carried out.

#### Transmission Control

Via the ``transmission_control`` parameter in the model configuration, the cell-specific state ``p_transmit`` can be manipulated. The cell state manipulation happens *before* the update rule above is carried out.

#### Data Output

The following data is stored alongside the simulation:

* ``kind``: the state of each cell:

   * ``0``: ``empty``
   * ``1``: ``susceptible``
   * ``2``: ``exposed``
   * ``3``: ``infected``
   * ``4``: ``recovered``
   * ``5``: ``deceased``
   * ``6``: ``source``, is constantly infectious
   * ``7``: ``inert``, does not take part in any interaction

* ``age``: the age of each cell, reset after a cell turns empty.
* ``cluster_id``: a number identifying to which cluster a cell belongs; is ``0`` for non-living cells. Recovered cells do not count towards it.
* ``exposed_time``: the time steps a living cell has already been exposed to the disease, for each cell.
* ``immunity``: whether or not a cell is immune, for each cell.
* ``densities``: the densities of each of the kind of cells over time; this is a labeled 2D array with the dimensions ``time`` and ``kind``.
* ``counts``: *cumulative* counters for a number of events, e.g. state transitions. This is a 2D array with the dimensions ``time`` and ``label``, where the latter describes the name of the counted event:

    * ``empty_to_susceptible``, i.e. "birth"
    * ``living_to_empty``, i.e. "random death"
    * ``susceptible_to_exposed_contact``, via local contact with a neighbor
    * ``susceptible_to_exposed_random``, via a random point exposure, controlled by ``p_exposed``
    * ``susceptible_to_exposed_controlled``, via exposure control.
    * ``exposed_to_infected``, as controlled by ``p_infected``
    * ``infected_to_recovered``, as controlled by ``p_recovered``
    * ``infected_to_deceased``, as controlled by ``p_deceased``
    * ``recovered_to_susceptible``, which happens when losing immunity, as controlled by ``p_lose_immunity``
    * ``move_randomly``, as controlled by ``p_move_randomly``
    * ``move_away_from_infected``, as enabled by ``move_away_from_infected``
