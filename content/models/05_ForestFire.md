---
date: 2020-04-16T

tagline: ForestFire
heading: Model of Forest Fires
subheading: >
  The `ForestFire` model simulates the development of a forest under influence of
  fires. Trees grow on a random basis and lightning causes them and the trees in the
  same cluster to burn down instantaneously; this is the so-called “two state model”.

type: models
highlight: true

cover_image: "images/models/ForestFire/ff_1.png"
video: "https://seafile.blsqr.xyz/d/3bb1ecb0f0364026bbc8/files/?p=/videos/ForestFire.mp4&raw=1"
project_images:
  images:
    - link: "images/models/ForestFire/ff_1.png"
      desc: "Forest state snapshot (grid resolution: 512 x 512)."
    - link: "images/models/ForestFire/ff_2.png"
      desc: |
        Forest age snapshot (grid resolution: 512 x 512). Darker spots represent
        older trees.
  desc: "`ForestFire` simulation output: forest state (left) and age (right)"

link_to_documentation: https://docs.utopia-project.org/html/models/ForestFire.html

---

#### Implementation

The forest is modelled as a cellular automaton where each cell has can be in one of two states: ``empty`` or ``tree``.

The update procedure is as follows: in each iteration step, iterate over all cells in a random order. For each cell, one of two actions can take place, depending on the current state of the cell:

* An ``empty`` cell becomes a ``tree`` with probability ``p_growth``.
* A ``tree`` ignites with a probability ``p_lightning`` and ignites all cells indirectly connected to it. The cluster burns down instantaneously and all cells transition to state ``empty``.

The new state of a cell is applied directly after it was iterated over, i.e. cell states are updated *sequentially*.

#### Heterogeneities

There is the possibility to introduce heterogeneities into the grid, which are implemented as two additional possible cell states: ``source`` and ``stone``:

* A cell can be a constantly ignited fire ``source``, instantly burning down a cluster that comes into contact with it.
* A cell can be a ``stone``, being immune to fire and not taking part in any model dynamics.

These heterogeneities are controlled via the ``ignite_permanantly`` and ``stones`` entries of the model configuration, which both make use of the [entity selection interface](https://docs.utopia-project.org/html/about/features.html#the-select-interface-selecting-entities-using-some-condition).

#### Data Output

The following data is stored alongside the simulation:

* ``kind``: the state of each cell. Possible values:

   * ``0``: ``empty``
   * ``1``: ``tree``
   * ``2``: (not used)
   * ``3``: ``source``, is constantly ignited
   * ``4``: ``stone``, does not take part in any interaction

* ``age``: the age of each tree, reset after lightning strikes
* ``cluster_id``: a number identifying to which cluster a cell belongs; ``0`` for non-tree cells
* ``tree_density``: the global tree density

---

**References**

* Per Bak, Kan Chen, and Chao Tang, **1990**: A forest-fire model and some thoughts on turbulence, Physics Letters A, 147, (5-6), 297–300, DOI: [10.1016/0375-9601(90)90451-S](https://doi.org/10.1016/0375-9601(90)90451-S).
* Barbara Drossel and Franz Schwabl, **1992**: Self-organized critical forest-fire model, Physical Review Letters, 69, 1629, DOI: [10.1103/PhysRevLett.69.1629](https://doi.org/10.1103/PhysRevLett.69.1629)
