---
tagline: "Utopia features"
heading: "Model building"
subheading: |
  Utopia provides powerful tools for implementing scientific computer models, starting from the infrastructure supplied by the `Model` base class and ranging to ready-to-use manager structures for implementing cellular automata or agent-based models.

  Here is an overview of some of its most useful model building features.
---
// for feature content, see data/features.yml
