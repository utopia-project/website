---
tagline: "Utopia features"
heading: "Simulation Control and Configuration"
subheading: |
  Setting up and performing simulations is a central part of the workflow when investigating computer models: it is the interface between the person investigating the model and the model itself.
  
  Utopia provides powerful tools to configure simulations and carry them out:
  With a versatile configuration system and easy parallelization mechanisms, it aims to make this process as convenient as possible, while maintaining a high degree of flexibility and ensuring reproducibility.
---
