---
tagline: "Utopia features"
heading: "Visualization"
subheading: |
  Visualization allows you intuitively grasp and present the model dynamics.
  
  Utopia provides a host of tools to this end, from animated graphs, agent plots, and cellular 
  automata, to 3-dimensional scatter plots, stacked lines and errorbars, heatmaps, all directly
  controllable and customisable from the config, and optimised for high-dimensional data. 
  You are also free to add your own custom plot functions: they will integrate seamlessly with the framework.

---
