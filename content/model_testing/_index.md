---
tagline: "Utopia features"
heading: "Model testing"
subheading: |
  With computer models as research objects, it becomes crucial to assure their reliable operation.
  Implementing model tests can not only help to detect implementation errors, but also assists in maintainable growth of the software project, which larger models inevitably become.

  Utopia facilitates testing models by making it easier to implement tests alongside a model and automating how they are carried out.
  While this cannot address the [test oracle problem](https://en.wikipedia.org/wiki/Test_oracle), it can address many potential difficulties.

# Utopia and Dantro are tested:
# Utopia: ![](https://gitlab.com/utopia-project/utopia/badges/master/coverage.svg)
# dantro: ![](https://gitlab.com/utopia-project/dantro/badges/master/coverage.svg)
---
// for feature content, see data/features.yml
