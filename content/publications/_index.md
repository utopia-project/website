---
title: "Publications"
heading : "Publications"
---

### About Utopia
When using Utopia for your own work, please consider citing these articles.

* Lukas Riedel, Benjamin Herdeanu, Harald Mack, Yunus Sevinchan, and Julian Weninger. 2020.  
  **Utopia: A Comprehensive and Collaborative Modeling Framework for Complex and Evolving Systems**.  
  *Journal of Open Source Software* 5 (53): 2165.
  DOI: [10.21105/joss.02165](https://doi.org/10.21105/joss.02165).

* Yunus Sevinchan, Benjamin Herdeanu, Harald Mack, Lukas Riedel, and Kurt Roth. 2020.  
  **Boosting Group-Level Synergies by Using a Shared Modeling Framework**.  
  *Computational Science – ICCS 2020*, edited by VV. Krzhizhanovskaya, G. Závodszky, MH. Lees, JJ. Dongarra, PMA. Sloot, S. Brissos, and J. Teixeira, 12143:442–456.
  Lecture Notes in Computer Science. Cham, Switzerland: Springer International Publishing.
  DOI: [10.1007/978-3-030-50436-6_32](http://doi.org/10.1007/978-3-030-50436-6_32)

* Yunus Sevinchan, Benjamin Herdeanu, and Jeremias Traub. 2020.  
  **dantro: a Python package for handling, transforming, and visualizing hierarchically structured data**.  
  *Journal of Open Source Software* 5 (52): 2316.
  DOI: [10.21105/joss.02316](https://doi.org/10.21105/joss.02316).


To download the corresponding **BibTeX data**, please refer to [this page in the project documentation](https://docs.utopia-project.org/html/cite.html).

---

### Using Utopia
The following works made substantial use of Utopia for the implementation, simulation, and analysis of computer models.

* Petro Sarkanych, Yunus Sevinchan, Marjana Krasnytska, Pawel Romanczuk, Yurij Holovatch. 2024.
  **Consensus decision making on a complete graph: complex behaviour from simple assumptions**,
  *Condensed Matter Physics*, 27(3).
  DOI: [10.5488/cmp.27.33801](https://doi.org/10.5488/cmp.27.33801)
  — [Model repository](https://gitlab.com/utopia-project/models/SocialSpinModels)

* Thomas Gaskin, Grigorios A. Pavliotis, Mark Girolami. 2024.
  **Inferring networks from time series: A neural approach**,
  *PNAS Nexus*, 3(4).
  DOI: [10.1093/pnasnexus/pgae063](https://doi.org/10.1093/pnasnexus/pgae063)
  — [utopya model repository](https://github.com/ThGaskin/NeuralABM)

* Thomas Gaskin, Tim Conrad, Grigorios A. Pavliotis, Christof Schütte. 2024.
  **Neural parameter calibration and uncertainty quantification for epidemic forecasting**,
  *PLOS ONE*, 19(10), e0306704.
  DOI: [10.1371/journal.pone.0306704](https://doi.org/10.1371/journal.pone.0306704),
  [arXiV preprint](https://doi.org/10.48550/arXiv.2312.03147)
  — [utopya model repository](https://github.com/ThGaskin/NeuralABM)

* Harald Mack. 2023.
  **Evolution from the ground up with Amee: from basic concepts to explorative modeling**,
  Doctoral dissertation. *heiDOK*. Heidelberg University.
  DOI: [10.11588/heidok.00033914](https://doi.org/10.11588/heidok.00033914)
  — [Model repository](https://gitlab.com/phd_project_1622/allkindsofamee)

* Julian Weninger. 2023.
  **Mechanical Insights into the Development of Positional and Orientational Order in Multi-Cell-Type Tissues**,
  Doctoral dissertation. Université de Genève.
  DOI: [10.13097/archive-ouverte/unige:174312](https://doi.org/10.13097/archive-ouverte/unige:174312)
  — [Model repository](https://gitlab.com/JulianWeninger/vertex)

* Raj Ladher, Anubhav Prakash, Julian Weninger, Nishant Singh, Sukanya Raman, Karsten Kruse, Madan Rao, 2023.
  **Junctional Force Patterning drives both Positional and Orientational Order in Auditory Epithelia**,
  *Research Square* preprint (v1).
  DOI: [10.21203/rs.3.rs-2508957/v1](https://doi.org/10.21203/rs.3.rs-2508957/v1)
  — [Model repository](https://gitlab.com/JulianWeninger/vertex)

* Thomas Gaskin, Grigorios A. Pavliotis, and Mark Girolami. 2023.
  **Neural parameter calibration for large-scale multiagent models**,
  *PNAS* 120 (7).
  DOI: [10.1073/pnas.2216415120](https://doi.org/10.1073/pnas.2216415120)
  — [utopya model repository](https://github.com/ThGaskin/NeuralABM)

*  Benjamin Herdeanu. 2021.
  **Emergence of Cooperation in Evolutionary Social Interaction Networks**,  
  Doctoral dissertation. *heiDOK*. Heidelberg University.
  DOI: [10.11588/heidok.00030725](https://doi.org/10.11588/heidok.00030725)
  — [Model repository](https://gitlab.com/herdeanu/utopia-models)

*  Yunus Sevinchan. 2021.
  **Evolution Mechanics and Perspectives on Food Web Ecology**,  
  Doctoral dissertation. *heiDOK*. Heidelberg University.
  DOI: [10.11588/heidok.00030750](https://doi.org/10.11588/heidok.00030750).
  — [Model repository](https://gitlab.com/utopia-project/models/blsqr-evo-eco)
