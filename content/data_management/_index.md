---
tagline: "Utopia features"
heading: "Data management"
subheading: |
  Utopia comes with its own object-oriented C++ HDF5 library which takes care of storing the model state into an efficient, 
  open, and frequently-used data format.

  For storing data of models with a non-adaptive state size, Utopia aims to makes the data-writing interface as 
  simple as possible.
  However, adaptively-sized models require more elaborate data writing procedures; the `DataManager` aims to cover 
  this area and provide a high flexibility.
---
// for feature content, see data/features.yml
