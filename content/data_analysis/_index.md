---
tagline: "Utopia features"
heading: "Data analysis"
subheading: |
  Investigating a model's data output requires handling vast amounts of data efficiently and reliably.

  We developed the [`dantro`](https://dantro.readthedocs.io/en/stable/index.html) Python package for this task and integrated it into Utopia.
  `dantro` implements a **data processing pipeline** which streamlines *handling, transforming, and visualizing* data via a sequence of configurable operations.
  Same as with the Utopia project, the package aims at improving the scientific workflow's efficiency, reliability, and reproducibility.

---
// for feature content, see data/features.yml
