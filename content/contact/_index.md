---
title         : "About us"
heading       : Feel free to get in touch.
description   : |
  Utopia is a collaborative tool for researchers, and we welcome
  you to join in its development.
  Visit the [GitLab project](https://gitlab.com/utopia-project/utopia) site to file issues or request access to become a developer.
  Or [get in touch with us](mailto:utopia-dev@iup.uni-heidelberg.de) directly.
button_1:
  text: GitLab&nbsp;project
  URL: https://gitlab.com/utopia-project/utopia
button_2:
  text: Message&nbsp;us
  URl: mailto:utopia-dev@iup.uni-heidelberg.de
---
