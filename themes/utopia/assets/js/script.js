$(document).ready(function() {
  "use strict";

  // Smooth scroll
  $('a.scroll-to').on('click', function (event) {
    var $anchor = $(this);
    event.preventDefault();
    $('html, body').stop().animate({
        scrollTop: ($($anchor.attr('href')).offset().top - 50)
    }, 500);
  });

  // Highlight current navbar item
  (function() {
      const links = document.getElementsByTagName('a');
      const currentUrl = location.href;
      for (const link of links) {
          if ((link.href === currentUrl) && (link.id == "navlink")){
              link.classList.add('nav-active')
          }
      }
  }())

});

// Change navbar while scrolling
$(window).on('scroll', function () {
  var windscroll = $(window).scrollTop();
  if (windscroll >= 100) {
    $('.nav').addClass('nav-scroll');
  } else {
    $('.nav').removeClass('nav-scroll');
  }
});

// Open external links in new tab, with the exception of emails
$('a').each(function() {
   var a = new RegExp('/' + window.location.host + '/');
   if(!a.test(this.href)) {
       $(this).click(function(event) {
         if (this.href.indexOf("mailto") > -1) {
           window.open(this.href, '_self');
         }
         else {
           event.preventDefault();
           event.stopPropagation();
           window.open(this.href, '_blank');
         }
       });
   }
});

jQuery('img.svg').each(function(){
    var $img = jQuery(this);
    var imgID = $img.attr('id');
    var imgClass = $img.attr('class');
    var imgURL = $img.attr('src');

    jQuery.get(imgURL, function(data) {
        // Get the SVG tag, ignore the rest
        var $svg = jQuery(data).find('svg');

        // Add replaced image's ID to the new SVG
        if(typeof imgID !== 'undefined') {
            $svg = $svg.attr('id', imgID);
        }
        // Add replaced image's classes to the new SVG
        if(typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass+' replaced-svg');
        }

        // Remove any invalid XML tags as per http://validator.w3.org
        $svg = $svg.removeAttr('xmlns:a');

        // Replace image with new SVG
        $img.replaceWith($svg);

    }, 'xml');

});

$(document).ready(function () {
    $('.carousel').carousel({
        interval: 7000
    });

    $('.carousel').carousel('cycle');
});

// scroll to accordion entry on load thru external link
$(window).on('load', function () {
    if (window.location.hash) {
      var accordion = document.getElementById(window.location.hash.substring(1)+'_accordion');

      $('html, body').stop().animate({
          scrollTop: ($(accordion.getElementsByClassName('card-link')).offset().top - 100)
      }, 300);
      $(accordion.getElementsByClassName('card-link')).trigger("click");
    }
});
